import { Player } from './class/Player.js';
import { Mob } from './class/Mob.js';
import { Item } from './class/Item.js';
import { Grid } from './class/Grid.js';



export function gameOver(){

    let leaderboard = JSON.parse(localStorage.getItem('leaderboard')) || [];
    let infoPlayer = JSON.parse(localStorage.getItem('information'));
    leaderboard.push(infoPlayer);

    leaderboard.sort(function(a, b) {
        return b.score - a.score;
    });

    localStorage.setItem('leaderboard', JSON.stringify(leaderboard));

        window.location.href = "gameover.html"; 

}

export function getRandom(min, max) {

    return Math.floor(Math.random() * ((max + 1) - min) + min);
}

export function newLevel(level, player){

    //définition des éléments utilisés pour créer le jeu
    const grid = document.getElementById('grid');

    let sizeSquare = 50;
    let hp = 12, energy = 250, money = 0, damage = 1, shuriken = 3;

    //création de la map
    let map = new Grid(grid, sizeSquare);
    
    map.load(level); //charge la map   
    map.loadStare(); //charge l'escalier
    map.draw(); //affichage de la map

    let contentMap = map.contentMap;

    //met à jour taille de la map si besoin
    let nbrSquare = map.nbrSquare;
    sizeSquare = map.sizeSquare;

    //trouve la case de départ du joueur
    let playerSquare;
    do {
        playerSquare = getRandom(0, nbrSquare * nbrSquare - 1);
    }
    while (contentMap[playerSquare] !== 0)

    grid.style.height = map.nbrSquare * sizeSquare + "px";
    grid.style.width = map.nbrSquare * sizeSquare + "px";

    moveView(grid, playerSquare, nbrSquare, sizeSquare);

    //crée notre joueur si c'est le premier niveau, sinon met à jour ses informations
    if(level === 1){
        player = new Player(grid, playerSquare, hp, energy, money, damage, shuriken, 'up', map.nbrSquare, sizeSquare, contentMap);
    } else {
        player.oldSquare = player.square;
        player.square = playerSquare;
        player.contentMap + contentMap;
        player.level = level;
        player.nbrSquare = nbrSquare;
        player.sizeSquare = sizeSquare;
    }   

    player.draw();  //affiche le joueur  

    //crée les mobs
    let valueMobs = createMob(level, grid, nbrSquare, sizeSquare, contentMap, playerSquare);
    
    //crée les items
    let items = createItem(level, grid, nbrSquare, sizeSquare, contentMap, valueMobs.squareTaken);

    return { player, valueMobs, items, map };
}

function createMob(level, grid, nbrSquare, sizeSquare, contentMap, playerSquare){
    const mobs = {};
    const ratioMob = 12;
    let nbrMob = [];
    let squareTaken = [];

    //ne pas créer un mobs proche du joueur
    squareTaken.push(playerSquare);
    squareTaken.push(playerSquare + 1);
    squareTaken.push(playerSquare - 1);
    squareTaken.push(playerSquare - nbrSquare);
    squareTaken.push(playerSquare + nbrSquare);

    nbrMob.push(ratioMob - level * 2); //diminue en fonction du level
    nbrMob.push((level - 1) * 2); // 2 supplémentaires par niveau à partir du 2e etage
    nbrMob.push(level - 1); // 1 supplémentaire par niveau à partir du 2e etage
    nbrMob.push((level - 2) * 2); // 2 supplémentaires par niveau à partir du 3e etage
    nbrMob.push(level - 2); // 1 supplémentaire par niveau à partir du 3e etage
    nbrMob.push(level - 3); // 1 supplémentaire par niveau à partir du 4e etage

    //pour chaque niveau de monstre, on ajoute au tableau
    for(let id = 0 ; id < nbrMob.length ; id++){
        if(nbrMob[id] > 0){
            for (let i = 0; i < nbrMob[id]; i++) {
                let mob = new Mob((id + 1), grid, nbrSquare, sizeSquare, contentMap, squareTaken);
                    mob.draw();  
                    squareTaken.push(mob.square);                       
                mobs[mob.square] = mob;
            }
        }
        
    }
    //enlève les square proche du joueur
    squareTaken.splice(1, 4);

    return { mobs, squareTaken };        
}

function createItem(level, grid, nbrSquare, sizeSquare, contentMap, squareTaken){
    let items = {}, random, oneItem;
    let idItem = [], squareItem = [];
    const nbrItem = 3;

    //chargement des objets très rare
    if(getRandom(1, 500) === 1)
    {
        console.log("super rare");
        switch(getRandom(1, 4))
        {
            //8000 score
            case 1: 
            default:
                idItem.push(11);
                break;

            //bombe
            case 2: 
                idItem.push(14);
                break;

            //katana
            case 3: 
                idItem.push(16);
                break;

            //trésor
            case 4: 
                idItem.push(5);
                break;

        }
    }
    
    //chargement des objets rare
    for (let i = 0; i < 2; i++) {
        
        if(getRandom(1, 50) === 1)
        {
            console.log("rare");
            switch(getRandom(1, 9))
            {
                //scroll feu
                case 1: 
                    idItem.push(20);
                    break;
    
                //scroll tp
                case 2: 
                    idItem.push(21);
                    break;
    
                //10 shurikens
                case 3: 
                    idItem.push(18);
                    break;

                //knife
                case 4: 
                    idItem.push(15);
                    break;

                //potion carré
                case 5: 
                    idItem.push(13);
                    break;

                //3000 score
                case 6: 
                    idItem.push(10);
                    break;

                //coussin
                case 7: 
                    idItem.push(8);
                    break;
    
                //meat
                case 8: 
                    idItem.push(6);
                    break;
    
                //big gold
                case 9: 
                default:
                    idItem.push(4);
                    break;    
            }
        }
    }

    //chargement des objets commun
    for (let i = 0; i < 5; i++) {
        
        if(getRandom(1, 5) === 1)
        {
            console.log("commun");
            switch(getRandom(1, 6))
            {
                //3 shuriken
                case 1: 
                    idItem.push(17);
                    break;
    
                //potion normal
                case 2: 
                    idItem.push(12);
                    break;
    
                //1000 score
                case 3: 
                    idItem.push(9);
                    break;

                //pomme
                case 4: 
                case 5: 
                    idItem.push(7);
                    break;

                //gold standard
                case 6: 
                default:
                    idItem.push(3);
                    break;  
            }
        }
    }

    //chargement de pièces
    for (let i = 0; i < nbrItem; i++)
    {
        random = getRandom(1, 100);
        switch(true)
        {
            //trésor
            case (random === 1): 
                idItem.push(5);
                break;

            //big piece
            case (random <= 20): 
                idItem.push(4);
                break;

            //piece normal
            case (random <= 60): 
                idItem.push(3);
                break;

            //petite pièce
            case (random <= 100): 
            default:
                idItem.push(2);
                break;
        }
    }

    //pour chaque id on le crée et l'ajoute au tableau
    idItem.forEach(id => {
        oneItem = new Item(grid, id,  nbrSquare, sizeSquare, contentMap, squareTaken, squareItem);
        items[oneItem.square] = oneItem;
        squareItem.push(oneItem.square); 

        oneItem.draw();
    });
    return items;
}

export function findPosFromSquare(square, nbrSquare, sizeSquare){
    let x = (square % nbrSquare) * sizeSquare ;
    let y = Math.floor(square / nbrSquare) * sizeSquare;
    return {x, y};
}

export function moveView(grid, playerSquare, nbrSquare, sizeSquare){
    const view = document.getElementById('view');   
    const ratio = 3;
    let pos = findPosFromSquare(playerSquare, nbrSquare, sizeSquare);

    //met à jour la view et la grid de notre jeu
    const minOffset = -(nbrSquare * sizeSquare) + ((ratio * 2 + 1) * sizeSquare);
    const offsetX = Math.max(Math.min(0, (ratio * sizeSquare - pos.x)), minOffset);
    const offsetY = Math.max(Math.min(0, (ratio * sizeSquare - pos.y)), minOffset);

    view.style.height = (ratio * 2 + 1) * sizeSquare + "px";
    view.style.width = (ratio * 2 + 1) * sizeSquare + "px";

    grid.style.top = offsetY + "px";
    grid.style.left = offsetX + "px";
}