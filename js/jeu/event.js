import { getRandom, newLevel, moveView} from './global-function.js';

export function eventGame(player, mobs, squareTaken, map, items){

    let slots = document.querySelectorAll(".items-case");

    slots.forEach(function(element){
        element.addEventListener('click', (event) => {
            mobs = useSlotItem(slots, element, player, mobs, items);
        });
    });

    window.addEventListener('keydown', (event) => {

        event.preventDefault();

        if(!player.isActive)
        {       
            let direction = false, isMoving = false;
            const grid = document.getElementById("grid");
            const view = document.getElementById("view");
            const regex = /-?[0-9]+/g;   
        
            switch (event.key) {
                case "ArrowUp":
                    case "z":
                    direction = "up";
                    break;
                case "ArrowDown":
                    case "s":
                    direction = "down";
                    break;
                case "ArrowLeft":
                    case "q":
                    direction = "left";
                    break;
                case "ArrowRight":
                    case "d":
                    direction = "right";
                    break;
                case " ":
                    player.useShuriken(mobs);
                    break;
                default:
                    break;            
            }

            //si une touche direction est appuyée
            if(direction){            
                isMoving = player.move(direction, mobs, items); //variable pour contenir le movement du joueur (oui ou non)
            }

            //si le joueur bouge, on va faire bouger les mobs
            if(isMoving)
            {
                //après avoir bougé, s'il est sur l'escalier, change le niveau
                if(player.contentMap[player.square] === 2)
                {
                    map.delete(player.square); //on vide la grille et on enlève l'escalier
                    let objectValue = newLevel(player.level + 1, player);
                    //mise à jour des informations
                    player = objectValue.player;
                    mobs = objectValue.valueMobs.mobs;
                    items = objectValue.items;
                    map = objectValue.map;
                    squareTaken = objectValue.valueMobs.squareTaken;
                    let up = new Audio("./sound/door.wav");
                    up.volume = 0.2;
                    up.play();
                    
                } else {

                    //si le joueur effectue une action et n'est pas en combat, on bouge la grille
                    if(!player.inFight) {
                        let offsetTop = parseInt(grid.style.top.match(regex)[0]);
                        let offsetLeft = parseInt(grid.style.left.match(regex)[0]);
                        let gridHeight = parseInt(grid.style.height.match(regex)[0]);
                        let gridWidth = parseInt(grid.style.width.match(regex)[0]);
                        let viewHeight = parseInt(view.style.height.match(regex)[0]);
                        let viewWidth = parseInt(view.style.width.match(regex)[0]);
                        let offsetSquare = 4;
                        switch (player.direction) 
                        {
                            case "up":
                                //TODO verif position player
                                if(offsetTop < 0 && player.square < (player.contentMap.length - player.nbrSquare * offsetSquare))
                                {
                                    grid.style.top = offsetTop + player.sizeSquare + "px";      
                                }                           
                                break;
                            case "down":
                                if(offsetTop > (viewHeight - gridHeight) && player.square > player.nbrSquare * offsetSquare){
                                    grid.style.top = offsetTop - player.sizeSquare + "px";  
                                }
                                break;
                            case "left":
                                if(offsetLeft < 0 && player.square % player.nbrSquare < player.nbrSquare - offsetSquare)
                                {
                                    grid.style.left = offsetLeft + player.sizeSquare + "px";
                                }
                                break;
                            case "right":
                                if(offsetLeft > (viewWidth - gridWidth) && player.square % player.nbrSquare >= offsetSquare){
                                    grid.style.left = offsetLeft - player.sizeSquare + "px";
                                }                            
                                break;
                            default:
                                break;            
                        }
                    }
                    squareTaken = updateSquareTaken(squareTaken, player.oldSquare, player.square);
                    mobs = moveMobs(mobs, squareTaken, player);
                } 
                player.inFight = false; //le joueur n'est plus en combat
            }
        }

    });
}

function moveMobs(mobs, squareTaken, player)
{
    let oldSquare;
    let newMobs = {};

    //on lit les valeurs de l'objet mobs
    Object.values(mobs).forEach(function(mob){
        
        oldSquare = mob.square;
        mob.move(squareTaken, player);
        squareTaken = updateSquareTaken(squareTaken, oldSquare, mob.square);
        newMobs[mob.square] = mob;
    });

    return newMobs;
}

function updateSquareTaken(squareTaken, oldSquare, newSquare){

     //on met à jour le tableau de case occupée
     let pos = squareTaken.indexOf(oldSquare);
           
     squareTaken.splice(pos, 1); //on vire l'ancien square du joueur
     squareTaken.push(newSquare); //on ajoute le nouveau square du joueur

     return squareTaken;
}

function useSlotItem(slots, element, player, mobs, items){

    if(element.querySelector("img") !== null)
    {
        let arr = Array.prototype.slice.call(slots); //transforme le nodelist en tableau
        let index = arr.indexOf(element);

        if(player.items[index])
        {
            let item = player.items[index];
            let refreshDashboard = false, refreshSlot = false;
            switch(item.id){

                //pillow
                case 8:
                    const pillowUse = 10;
                    //si le joueur à assez de nourriture et pas les hp max
                    if(player.food >= pillowUse && player.health < player.maxHealth)
                    {
                        player.food -= pillowUse; //enlève la nourriture
                        player.setHealth(1); //rajoute 1 de vie
                        refreshDashboard = true;
                    }
                    break;

                //potion ronde
                case 12:

                    //si le joueur n'a pas les hp max
                    if(player.health < player.maxHealth)
                    {
                        player.setHealth(item.value); //rajoute de la vie
                        refreshDashboard = true;
                        refreshSlot = true;
                    }
                    break;

                //potion carré
                case 13:

                    //si le joueur n'a pas les hp max
                    if(player.health < player.maxHealth)
                    {
                        player.maxHealth += 2;
                        player.setHealth(item.value); //rajoute de la vie
                        refreshDashboard = true;
                        refreshSlot = true;
                    }
                    break;

                //bombe
                case 14:

                    //si l'existe un monstre sur la map
                    if(mobs)
                    {
                        //pour chaque monstre
                        for (const [key, value] of Object.entries(mobs)) {
                          
                            player.score += value.score;
                            value.deleteElement(".mob"); //on nettoie la case du mob
                            value.drawDamage(value.square, 99);
                        }

                        refreshDashboard = true;
                        refreshSlot = true;
                        mobs = {};
                    }
                    break;

                //scroll fire
                case 20:

                    let squareMob = [
                        (player.square - player.nbrSquare),
                        (player.square + player.nbrSquare),
                        (player.square + 1),
                        (player.square - 1)
                     ];

                    //pour chaque monstre
                   squareMob.forEach(function(square) {
                        
                        if(mobs[square]){
                            player.score += mobs[square].score;
                            mobs[square].deleteElement(".mob"); //on nettoie la case du mob
                            mobs[square].drawDamage(square, 99);
                            delete mobs[square];
                        }                            
                    });

                    refreshDashboard = true;
                    refreshSlot = true;

                    break;

                //scroll tp
                case 21:

                    let square; 
                    /* on fait une boucle pour trouver une case libre 
                    pas de murs
                    pas d'items
                    pas de monstres à une case de distance
                    */
                   do {
                       //on fait une boucle pour trouver une case qui n'est pas un mur
                        do {
                            square = getRandom(0, player.nbrSquare * player.nbrSquare - 1);
                        } while(player.contentMap[square] !== 0)
                   } while(items[square] || mobs[square] || mobs[square - player.nbrSquare]
                    || mobs[square + player.nbrSquare] || mobs[square + 1] || mobs[square - 1]
                    || mobs[square - player.nbrSquare + 1] || mobs[square - player.nbrSquare - 1]
                    || mobs[square + player.nbrSquare + 1] || mobs[square + player.nbrSquare - 1])

                    player.deleteDamage();
                    //met à jour infos player au déplacement, et redessine
                    player.moveElement(".player", player.square, square);

                    player.oldSquare = player.square;
                    player.square = square;
                    moveView(player.grid, square, player.nbrSquare, player.sizeSquare);
                    refreshDashboard = true;
                    refreshSlot = true;
                    break;
            }

            //s'il faut mettre à jour le dashboard
            if(refreshDashboard) player.displayDashboard();

            //s'il faut mettre à jour les slots
            if(refreshSlot) {
                player.items.splice(index, 1); //supprime l'item
            
                //réaffiche slots
                player.deleteSlot();
                player.displaySlot(); 
            }
            
        }
    }
    return mobs;
}