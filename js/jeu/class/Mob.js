import {getRandom} from '../global-function.js';
import { ObjectMap } from './ObjectMap.js';

export class Mob extends ObjectMap {

    // caractéristique des minions.
    constructor(id, grid, nbrSquare, sizeSquare, contentMap, squareTaken ) {
        let square;
         //spawn le monstre seulement sur le sol et sur une case non occupée
         do {
            square = getRandom(0, (nbrSquare * nbrSquare - 1));
        } while(contentMap[square] !== 0 || squareTaken.includes(square))

        super(grid, nbrSquare, sizeSquare, contentMap, square)
        this.id = id;
        this.direction = "up";

        this.height = sizeSquare;
        this.width = sizeSquare;
        
        //on affecte des valeurs selon l'id
        switch(id){
            case 1:
                this.height = sizeSquare * 0.6;
                this.width = sizeSquare * 0.6;
                this.health = 2;
                this.damage = 1;
                this.score = 250;
                this.scope = 0; //range de vision des ennemis
                this.image = "img/mobs/salameche.png";
                break;
            case 2:
                this.height = sizeSquare * 0.8;
                this.width = sizeSquare * 0.8;
                this.health = 4;
                this.damage = 2;
                this.score = 500;
                this.scope = 1; //range de vision des ennemis
                this.image = "img/mobs/reptincel.png";
                break;
            case 3:
                this.health = 6;
                this.damage = 3;
                this.score = 750;
                this.scope = 2; //range de vision des ennemis
                this.image = "img/mobs/dracaufeu2.png";
                break;
            case 4:
                this.health = 2;
                this.damage = 5;
                this.score = 1000;
                this.scope = 1; //range de vision des ennemis
                this.image = "img/mobs/megaY.png";
                break;
            case 5:
                this.health = 10;
                this.damage = 4;
                this.score = 1500;
                this.scope = 3; //range de vision des ennemis
                this.image = "img/mobs/megaX.png";
                break;
            case 6:
                this.health = 15;
                this.damage = 6;
                this.score = 2000;
                this.scope = 4; //range de vision des ennemis
                this.image = "img/mobs/gigamax.png";
                break;
        }
    }

    draw() {
        const squareElement = this.getSquare(this.square);
       
        let div = document.createElement("span");
        div.style.top =  (this.sizeSquare - this.height) / 2 + "px";
        div.style.left =  (this.sizeSquare - this.width) / 2 + "px";

        
        let img = new Image();   
        img.width = this.width;
        img.height = this.height; 
        img.src = this.image;
        
        div.classList.add("mob");
        div.appendChild(img);
        squareElement.appendChild(div);
    }

    //bouge le mobs sur la map
    move(squareTaken, player){ 

        let direction = this.getPlayerIsBeside(player.square);
        if(direction)
        {     
            //enlève les pv du joueur
            let dmg = getRandom(0, this.damage);
            player.setHealth(-dmg);
            this.direction = direction;
            this.drawDamage(player.square, dmg); //on affiche les dmg
            if(dmg > 0){
                this.damageframe();
                let mobhit = new Audio("./sound/hit/mobhit.mp3");
                mobhit.volume = 0.1;
                mobhit.play();
                
            }
        } else {           
            this.chooseDirection(squareTaken);            
        }
    }

    damageframe(){
        var divRed = document.querySelector('.red-frame'); 
        divRed.style.zIndex =30;
        setTimeout(function(){ 
            divRed.style.zIndex = 0;
        }, 1);
    }

    //change les pv
    setHealth(healthAdd){
        this.health += healthAdd;
    }

    chooseDirection(squareTaken){
        const maxIdDirection = 3;
        let currentSquare;
        let idDirection = (getRandom(0, maxIdDirection)); //notre direction de départ, random
        let count = 0; //notre nombre d'itération
        //on choisis une direction, et on regarde si la case est dispo
        do {   
            currentSquare = this.square;   
            switch(idDirection)
            {
                //up
                case 0:
                    currentSquare = currentSquare - this.nbrSquare;
                    break;
                //down
                case 1:
                    currentSquare = currentSquare + this.nbrSquare;
                    break;
                //left
                case 2:
                    // on autorise à aller à gauche à l'exception de la dernière colonne
                    if (currentSquare % this.nbrSquare !== 0 ){
                        currentSquare = currentSquare - 1;
                    }
                    break;
                //right
                case 3:
                    // on autorise à aller à droite à l'exception de la dernière colonne
                    if ((currentSquare+1) % this.nbrSquare !== 0 ){
                        currentSquare = currentSquare + 1;
                    }
                    break;
            }
            idDirection++; //on increment l'id direction
            count++; //augmente le count

            //si l'id direction est supérieur à son max, on le retombe à 0
            if(idDirection > maxIdDirection)
            {
                idDirection = 0;
            }
        
        } while((this.contentMap[currentSquare] !== 0 || squareTaken.includes(currentSquare) || currentSquare < 0 && currentSquare > (this.nbrSquare * this.nbrSquare - 1)) && count <= 4 )
        
        //si le mob peut bouger, on le fait bouger
        if(count <= 4)
        {
            this.moveElement(".mob", this.square, currentSquare);    
            this.direction = this.getNumDirection(idDirection);
            this.square = currentSquare; //notre carré est égal à notre nouvelle position
            this.draw();
        }
    }

    //regarde si le joueur est à porter de degats (et à côté sur la map, pas en bord de map ailleurs)
    getPlayerIsBeside(playerSquare)
    {        
        if(this.square === playerSquare - this.nbrSquare){
            return "up";
           
        } else if(this.square === playerSquare + this.nbrSquare){
            return "down";
        } else if(this.square === playerSquare + 1 && this.square % this.nbrSquare !== 0){
            return "left";
        } else if(this.square === playerSquare - 1 && (this.square + 1) % this.nbrSquare !== 0)
        {
            return "right";
        } else {
            return false;
        }
    }

    getNumDirection(idDirection)
    {
        switch(idDirection)
        {
            //up
            case 0:
                return "up";
            //down
            case 1:
                return "down";
            //left
            case 2:
                return "left";
            //right
            case 3:
                return "right";
        }
    }
}
