import { getRandom} from '../global-function.js';

    const map1 = [];
    map1.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0); //1
    map1.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map1.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map1.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map1.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0); //5
    map1.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map1.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map1.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map1.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map1.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0); //10
    map1.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map1.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);

    const map2 = [];
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map2.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0); 
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map2.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0); 
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0);
    map2.push(0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map2.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0); 
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map2.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0); 
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0);
    map2.push(0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);
    map2.push(0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0);

    export class Grid {        
        constructor(grid, sizeSquare) {

            this.grid = grid;
            this.sizeSquare = sizeSquare;
            this.contentMap = [];
        }

        //charge la map
        load(level){
            switch(level){
                case 1:
                    this.contentMap = map1;
                    break;
                case 2:
                    this.contentMap = map1;
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    this.contentMap = map1;
                    break;
            }
            this.nbrSquare = Math.floor(Math.sqrt(this.contentMap.length));
            
        }

        loadStare(){
            let square;
            do {
                square = getRandom(0, (this.nbrSquare * this.nbrSquare - 1));
            } while(
                this.contentMap[square] !== 0 || 
                (this.contentMap[square + 1] === 1 && this.contentMap[square - 1] === 1) || 
                (this.contentMap[square + this.nbrSquare] === 1 && this.contentMap[square - this.nbrSquare] === 1)
            )
            this.contentMap[square] = 2;
        }

        draw() {

            let x = 0;
            let y = 0;
            let self = this; //pour accéder a this dans la fonction du foreach

            let div = [];  //tableau des div          

            //index = square
            this.contentMap.forEach(function(item, square) {

                //on crée l'élément et on lui donne des infos
                div.push(document.createElement("div"));  
                div[square].style.width =  self.sizeSquare + "px";  
                div[square].style.height =  self.sizeSquare + "px";

                x = (square % self.nbrSquare) * self.sizeSquare;
                y = Math.floor(square / self.nbrSquare) * self.sizeSquare

                if (item === 0){                    
                    div[square].classList.add("floor");                    
                } else if (item === 1){
                    div[square].classList.add("wall");
                } else if (item === 2){
                    div[square].classList.add("stare");              
                }
                div[square].style.top = y + "px";
                div[square].style.left = x + "px";
                self.grid.appendChild(div[square]);
            });
        }

        delete(stare){
            this.contentMap[stare] = 0;
            this.grid.innerHTML = "";
        }
    }  