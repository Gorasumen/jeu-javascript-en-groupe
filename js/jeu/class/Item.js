import { getRandom } from "../global-function.js";
import { ObjectMap } from "./ObjectMap.js";



export class Item extends ObjectMap {
  // caractéristique des objets.
  constructor(grid, id, nbrSquare, sizeSquare, contentMap, squareTaken, squareItem) {
    let square;
    //spawn le monstre seulement sur le sol et sur une case non occupée
    do {
      square = getRandom(0, nbrSquare * nbrSquare - 1);
    } while (contentMap[square] !== 0 || squareTaken.includes(square) || squareItem.includes(square));

    super(grid, nbrSquare, sizeSquare, contentMap, square);
    this.id = id;

    this.use = true;
    this.cost = 0;
    this.value = 0;

    this.height = sizeSquare / 2;
    this.width = sizeSquare / 2;
 

    switch (id) {
      case 1:
        this.value = 3;
        this.use = false;
        this.img = "img/items/hearts.png";
        this.sound = "./sound/potion/heal.mp3";
        break;

      case 2:
        this.height = sizeSquare / 4;
        this.width = sizeSquare / 4;
        this.value = 1;
        this.use = false;
        this.img = "img/items/onegold.png";
        this.sound = "./sound/coins/biggold.mp3";
        break;

      case 3:
        this.height = sizeSquare / 4;
        this.width = sizeSquare / 2;
        this.value = 5;
        this.use = false;
        this.img = "img/items/threegold.png";
        this.sound = "./sound/coins/biggold.mp3";
        break;

      case 4:
        this.value = 20;
        this.use = false;
        this.img = "img/items/big_gold.png";
        this.sound = "./sound/coins/biggold.mp3";
        break; 
        
      case 5:
        this.height = sizeSquare * 0.40;
        this.width = sizeSquare * 0.60;
        this.value = 60;
        this.use = false;
        this.img = "img/items/treasure_chest.png";
        this.sound = "./sound/coins/treasurechest.mp3";
        break;


      case 6:
        this.value = 100;
        this.use = false;
        this.img = "img/items/meat.png";
        this.sound = "./sound/food/crunch7.ogg";
        break;

      case 7:
        this.value = 50;
        this.use = false;
        this.img = "img/items/apple.png";
        this.sound = "./sound/food/crunch5.ogg";
        break;
     
      case 8:
        this.img = "img/items/pillow.png";
        break;

      case 9:
        this.value = 1000;
        this.use = false;
        this.img = "img/items/emerald.png";
        break;

      case 10:
        this.value = 3000;
        this.use = false;
        this.img = "img/items/sapphire.png";
        break;

      case 11:
        this.value = 8000;
        this.use = false;
        this.img = "img/items/ruby.png";
        break;

      case 12:
        this.height = sizeSquare / 2;
        this.width = sizeSquare / 3;
        this.value = 8;
        this.img = "img/items/potion-round.png";
        this.sound = "./sound/coins/treasurechest.mp3";
        this.cost = 35;
        break;

      case 13:
        this.height = sizeSquare / 2;
        this.width = sizeSquare / 3;
        this.value = 999;
        this.img = "img/items/potion-square.png";
        break;

      case 14:
        this.img = "img/items/bomb.png";
        break;
      
      case 15:
        this.value = 2;
        this.use = false;
        this.img = "img/items/knife.png";
        this.cost = 80;
        break;

      case 16:
        this.height = sizeSquare * 0.8;
        this.width = sizeSquare * 0.4;
        this.value = 3;
        this.use = false;
        this.img = "img/items/katana.png";
        this.cost = 180;
        break;
      
      case 17:
        this.value = 3;
        this.use = false;
        this.img = "img/items/shuriken.png";
        this.cost = 15;
        break;

      case 18:
        this.value = 10;
        this.use = false;
        this.img = "img/items/big-shuriken.png";
        break;

      case 19:
        this.cost = 15;
        this.img = "img/items/bag.png";
        break;
      
      case 20:
        this.cost = 30;
        this.img = "img/items/scroll-fire.png";
        break;

      case 21:
        this.cost = 30;
        this.img = "img/items/scroll-tp.png";
        break;

    }
  }

  draw() {

    const squareElement = this.getSquare(this.square);

    let div = document.createElement("div");

    div.style.top =  (this.sizeSquare - this.height) / 2 + "px";
    div.style.left =  (this.sizeSquare - this.width) / 2 + "px";

    let img = new Image();

    img.width = this.width;
    img.height = this.height;

    img.src = this.img;
    div.classList.add("item");
    div.appendChild(img);

    squareElement.appendChild(div);
  }


}
