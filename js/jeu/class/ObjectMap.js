//La class parent des éléments de la map
export class ObjectMap {

    constructor (grid, nbrSquare, sizeSquare, contentMap, square) {
        this.grid = grid;
        this.nbrSquare = nbrSquare;
        this.sizeSquare = sizeSquare;
        this.contentMap = contentMap; 
        this.square = square;       
    }

    //change l'élément de la case
    moveElement(classCSS, oldSquare, newSquare)
    {    
        let oldSquareElement = this.getSquare(oldSquare);
        let element = oldSquareElement.querySelector(classCSS); 
        let newSquareElement = this.getSquare(newSquare);
        newSquareElement.appendChild(element);
        if(classCSS !== ".player"){
            element.remove();
        } 
        
    }

    //supprime l'élément de la case
    deleteElement(classCSS)
    {         
        let squareElement = this.getSquare(this.square);
        let element = squareElement.querySelector(classCSS); 
        element.remove(element);
    }

    //affiche les dmg
    drawDamage(square, damage)
    {
        const squareElement = this.getSquare(square);
        let element = document.createElement("span");  
        element.style.width = this.sizeSquare + "px";  
        element.style.height = this.sizeSquare + "px";
        element.classList.add("damage");  
        let textContent = document.createTextNode(damage);
        // ajoute le nœud texte au nouveau div créé
        element.appendChild(textContent);
        squareElement.appendChild(element);
    }

    //supprime les dmg
    deleteDamage(){
        document.querySelectorAll('.damage').forEach(function(element){
            element.remove();   
        });

    }

    //récupère l'élement html du square
    getSquare(square){
       
        return this.grid.querySelector("div:nth-child("+(square + 1)+")");
    }

}
