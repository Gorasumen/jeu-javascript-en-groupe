import { gameOver, getRandom, findPosFromSquare } from "../global-function.js";
import { Item } from "./Item.js";
import { ObjectMap } from "./ObjectMap.js";



export class Player extends ObjectMap {
  constructor(
    grid,
    square,
    health,
    food,
    money,
    damage,
    shuriken,
    direction,
    nbrSquare,
    sizeSquare,
    contentMap
  ) {
    super(grid, nbrSquare, sizeSquare, contentMap, square);
    this.health = health;
    this.maxHealth = health;
    this.food = food;
    this.money = money;
    this.damage = damage;
    this.shuriken = shuriken;
    this.direction = direction;
    this.directionShuriken = direction;
    this.isActive = false;
    this.level = 1; //level de départ
    this.slotMax = 3; //nombre de slots début de partie
    this.items = []; //notre liste d'objets
    this.score = 0;

    this.inFight = false;
    this.oldSquare = square;
  }

  draw() {
    const squareElement = this.getSquare(this.square);
    let element = document.createElement("span");
    element.style.width = "36px"; //this.sizeSquare + "px";
    element.style.height = "36px"; //this.sizeSquare + "px";
    element.classList.add("player");

    switch (this.direction) {
      case "up":
      default:
        element.classList.add("player-up");
        break;
      case "down":
        element.classList.add("player-down");
        break;
      case "left":
        element.classList.add("player-left");
        break;
      case "right":
        element.classList.add("player-right");
        break;
    }
    squareElement.appendChild(element);

    this.displayDashboard();
  }



  displayDashboard() {
    //afficher la vie et la nourriture
    let pv = document.querySelector(".pv > p > span");
    let gold = document.querySelector(".gold > p > span");
    let shuriken = document.querySelector(".shurikens > p > span");
    let food = document.querySelector(".food > p > span");
    let score = document.querySelector(".score > p > span");

    pv.innerHTML = this.health;
    gold.innerHTML = this.money;
    shuriken.innerHTML = this.shuriken;
    food.innerHTML = this.food;
    score.innerHTML = this.score;
  }

  changeDirection(newDirection) {
    let element = document.querySelector(".player");
    element.classList.replace(
      "player-" + this.direction,
      "player-" + newDirection
    );
    this.direction = newDirection;
  }

  move(direction, mobs, items) {
    let isMoving = false;
    this.isActive = true;

    let currentSquare = this.square;
    this.changeDirection(direction);
    switch (direction) {
      case "up":
        currentSquare = currentSquare - this.nbrSquare;
        isMoving = true;
        break;
      case "down":
        currentSquare = currentSquare + this.nbrSquare;
        isMoving = true;
        break;
      case "left":
        // on autorise à aller à gauche à l'exception de la dernière colonne
        if (currentSquare % this.nbrSquare !== 0) {
          currentSquare = currentSquare - 1;
          isMoving = true;
        }
        break;
      case "right":
        // on autorise à aller à droite à l'exception de la dernière colonne
        if ((currentSquare + 1) % this.nbrSquare !== 0) {
          currentSquare = currentSquare + 1;
          isMoving = true;
        }
        break;
    }

    //création hitbox si 0 = déplacement, sinon pas de déplacement
    if (this.contentMap[currentSquare] === 1) {
      isMoving = false;
      let hitwall = new Audio("./sound/hit/hitwall.wav");
      hitwall.volume = 0.1;
      hitwall.play()
      
    }

    //vérifie qu'on ne se déplace pas sur un ennemi
    if (mobs[currentSquare]) {
      this.inFight = true;
      var dmg = this.getDamage();
      let hitmob = new Audio("./sound/hit/hitmob.flac");
        hitmob.volume = 0.1;
        hitmob.play()
       

      //on delete le mobs du tableau si ses pv sont en dessous de 1
      this.fight(mobs, currentSquare, dmg);

      currentSquare = this.square; //on reste sur la case
      isMoving = true; //on veut qui se déplace
    }

    //on vérifie si tout est ok, et on peut se déplacer
    if (
      currentSquare >= 0 &&
      currentSquare <= this.nbrSquare * this.nbrSquare - 1 &&
      isMoving === true
    ) {
      this.deleteDamage();
      let footup = new Audio("./sound/footstep/footstep00.mp3");
      footup.volume = 0.1;

      footup.play();
     
      if (items[currentSquare] && !this.inFight) {
        let item = items[currentSquare];
        delete items[currentSquare]; //delete l'item du tableau grid
        item.deleteElement(".item"); //delete l'item de l'affichage grid 
        //on delete le mobs du tableau si ses pv sont en dessous de 1
        if (item.use == true) {

          if (this.items.length < this.slotMax) {

            //ajoute un item au slot
            this.items.push(item);
            this.deleteSlot();
            this.displaySlot();

          } else {
              
              this.deleteSlot(); //delete des slots
              
              this.items[0].square = currentSquare; //change le square de l'item qui va être remis sur la grid
              items[currentSquare] = this.items[0]; //ajoute l'item au tableau grid
              items[currentSquare].draw(); //affiche le nouvel item sur le current square
              this.items.splice(0, 1); //supprime le premier item du joueur
              this.items.push(item); //ajoute le nouvel
              
              //réaffiche slots
              this.displaySlot();             
          }
        } else {
          //consommer l'objet

          if(item.sound !== undefined){
            let sound =  new Audio(item.sound);
            sound.volume = 0.1;
            sound.play();
          }
         
          switch (item.id) {
            case 1:
              this.setHealth(item.value);
              break;

            case 2:
            case 3:
            case 4:
              this.money += item.value;
              break;

            case 5:
              this.money += item.value;
              break;

            case 6:
              this.food += item.value;
              break;
            
            case 7:
              this.food += item.value
            case 9:
            case 10:
            case 11:
              this.score += item.value;
              break;

            case 15:
            case 16:
              //met à jour la valeur des dmg (pas de diminution possible)
              this.damage = Math.max(this.damage, item.value);
              
              break;
            
            case 17:
            case 18:
              this.shuriken += item.value;
              break;
          }
        }
      }

      //met à jour infos player au déplacement, et redessine
      this.moveElement(".player", this.square, currentSquare);

      this.setFood(-1);
      this.oldSquare = this.square;
      this.square = currentSquare; //notre carré est égal à notre nouvelle position

      this.displayDashboard();

      this.isActive = false;
      return true;
    } else {
      this.isActive = false;
      return false;
    }
  }

  useShuriken(mobs)
  {
    //on vérifie qu'il possède des shurikens
    if(this.shuriken > 0){
      let square = this.findTarget(mobs);
      
      if(square !== undefined){
        this.deleteDamage();
        this.isActive = true;
        this.directionShuriken = square.dir;
        this.drawShuriken(mobs, square.num);
      }
    }
    
  }

  drawShuriken(mobs, square){
    const pos = findPosFromSquare(this.square, this.nbrSquare, this.sizeSquare);
    let element = document.createElement("span");
    element.classList.add("shuriken");
    element.style.top = pos.y  + this.sizeSquare / 4 + "px";
    element.style.left = pos.x + this.sizeSquare / 4 + "px";

    grid.appendChild(element);
    window.requestAnimationFrame(this.moveShuriken.bind(this, mobs, square));
  }

  moveShuriken(mobs, square)
  {
    let myReq;
    const regex = /-?[0-9]+/g;  
    let element = document.querySelector('.shuriken');
    let pos = findPosFromSquare(square, this.nbrSquare, this.sizeSquare)
    let offsetTop = parseInt(element.style.top.match(regex)[0]);
    let offsetLeft = parseInt(element.style.left.match(regex)[0]);
  
      switch(this.directionShuriken)
      {
        case "up":
          offsetTop -= 2;
          element.style.top = offsetTop + "px";
          break;
        case "down":
          offsetTop += 2;
          element.style.top = offsetTop + "px";
          break;
        case "left":
          offsetLeft -= 2;
          element.style.left = offsetLeft + "px";
          break;
        case "right":
          offsetLeft += 2;
          element.style.left = offsetLeft + "px";
          break;
      }   
    
    if((this.directionShuriken === "up" && offsetTop > pos.y + this.sizeSquare / 2) || (this.directionShuriken === "down" && offsetTop < pos.y + this.sizeSquare / 2)  || (this.directionShuriken === "left" && offsetLeft > pos.x + this.sizeSquare / 2) || (this.directionShuriken === "right" && offsetLeft < pos.x + this.sizeSquare / 2))
    {
      let myReq = window.requestAnimationFrame(this.moveShuriken.bind(this, mobs, square));
    }
    else {
     window.cancelAnimationFrame(myReq);
      var dmg = getRandom(1, 2); //dmg entre 1 et 2
      
      this.fight(mobs, square, dmg);
      element.remove();
      this.shuriken--;
      this.displayDashboard();
      this.isActive = false;
     
    }
  }

  findTarget(mobs){
    const range = 3; //range de portée des shurikens
    let down = true, up = true, left = true, right = true; //vérifie que la direction n'est pas bloqué par un mur
    let square; //le square calculé



    //on test la range, à partir de 2 (on lance pas de shuriken au cac)
    for(let i = 2 ; i <= range ; i++){

      //test dans la direction up
      square = this.square - this.nbrSquare  * i;
      if(square >= 0 && this.contentMap[square] !== 1 && up && mobs[this.square - this.nbrSquare] === undefined)
      {
        if(mobs[square] !== undefined)
        {
          
          return {num: square, dir: "up"};
        }
      } else {
        //si on rencontre un obstacle, on arrête de vérifier cette direction
        up = false;
      }

      //test dans la direction down
      square = this.square + this.nbrSquare  * i;
      if(square < this.nbrSquare * this.nbrSquare && this.contentMap[square] !== 1 && down && mobs[this.square + this.nbrSquare] === undefined)
      {
        if(mobs[square] !== undefined)
        {
          return {num: square, dir: "down"};
        }
      } else {
        down = false;
      }

      //test dans la direction left
      square = this.square - 1  * i;
      if(square % this.nbrSquare !== 0 && this.contentMap[square] !== 1 && left && mobs[this.square - 1] === undefined)
      {
        if(mobs[square] !== undefined)
        {
          return {num: square, dir: "left"};
        }
      } else {
        left = false;
      }

       //test dans la direction right
      square = this.square + 1  * i;
      if((square + 1) % this.nbrSquare !== 0 && this.contentMap[square] !== 1 && right && mobs[this.square + 1] === undefined)
      {
        if(mobs[square] !== undefined)
        {
          return {num: square, dir: "right"};
        }
      } else {
        right = false;
      }
    }
  }

  //change les pv
  setHealth(healthAdd) {
    this.health += healthAdd;

    if (this.health <= 0) {
      this.score += this.food * 5;
      this.score += this.money * 20;
      const infoPlayer = {
          pseudo: localStorage.getItem('pseudo'),
          score: this.score
      };
      localStorage.setItem('information', JSON.stringify(infoPlayer));
      gameOver();
    } else if(this.health > this.maxHealth){
        this.health = this.maxHealth;
    }
  }

  //change la nourriture
  setFood(foodAdd) {
    this.food += foodAdd;
    //si la nourriture descend en negatif, on enlève un pv
    if (this.food <= -1) {
      this.food = 0;
      this.setHealth(-1); //Perte d'un pv
    }
  }

  //enlève les pv d'un mob
  fight(mobs, square, dmg) {

    let mob = mobs[square];
    mob.setHealth(-dmg);
    this.drawDamage(square, dmg); //on affiche les dmg

    //on delete le mobs du tableau si ses pv sont en dessous de 1
    if (mob.health <= 0) {
      this.score += mob.score;
      mob.deleteElement(".mob"); //on nettoie la case du mob
      delete mobs[square];
    }

    return mobs;
  }

  //délisse les dmg, moins de 0, plus de gros degats (équilibrage)
  getDamage() {
    
    let chance = getRandom(0, 9);
    if (chance < 2) {
      return this.damage - 1;
    } else if (chance < 5) {
      return this.damage;
    } else if (chance < 8) {
      return this.damage + 1;
    } else {
      return this.damage + 2;
    }
  }

  deleteSlot() { 
    let slot = document.querySelectorAll('.items-case');

    slot.forEach(element => {
        let slotImg = element.querySelector('img'); 
        if (slotImg !== null){
           slotImg.remove(); 
        }
        
    });

  }

  displaySlot() {
    let slot = document.querySelectorAll('.items-case');
    this.items.forEach((element, index) => {
        let img = new Image();

        img.width = element.width;
        img.height = element.height;

        img.src = element.img;
        slot[index].appendChild(img);
    });

  }
}
