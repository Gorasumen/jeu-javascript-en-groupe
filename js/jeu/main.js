import { eventGame } from './event.js';
import { newLevel } from './global-function.js';

//sans onload ça le grid ne charge pas les changements
window.onload = function() {

    const level = 1;

    let value = newLevel(level, null);  
    eventGame(value.player, value.valueMobs.mobs, value.valueMobs.squareTaken, value.map, value.items);
  
}

