function addLeaderboard() {
    let apple;
    let leaderboard =  document.getElementById('leaderboard');
    const info = localStorage.getItem('leaderboard');
    let scoring = JSON.parse(info);
    const pseudo = localStorage.getItem('pseudo');
    

    for (const property in scoring) {
        if(pseudo == scoring[property].pseudo) {
            apple = 'class="apple"';
        } else {
            apple = '';
        }
        leaderboard.insertAdjacentHTML('beforeend', '<tr><td><span ' + apple + '>' + scoring[property].pseudo + '</span></td><td><span>' + scoring[property].score + '</span></td></tr>');
        
    } 
}

addLeaderboard();