//récupère le score du joueur et l'affiche
function getScore() {
    let span = document.getElementById('score');
    let infoPlayer = JSON.parse(localStorage.getItem('information')) || "";
    span.innerHTML = infoPlayer.score;
}

//restart le jeu
function eventRestart() {

    window.addEventListener('keydown', function(event) {
        if (event.key === 'r') {
           window.location.href = "jeu.html";
        }
    });
}


eventRestart();
getScore();