function btnStart() {
    let pseudo = document.querySelector("input");
    const button = document.querySelector(".button-start");

    button.addEventListener('click', function(e) {
        e.preventDefault();

        if(pseudo.value !== '')
        {
            localStorage.setItem('pseudo', pseudo.value);
            window.location.href = 'jeu.html';
        } 
        
    });
}

btnStart();
